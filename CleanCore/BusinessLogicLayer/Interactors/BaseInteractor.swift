//
//  BaseInteractor.swift
//  CleanCore
//
//  Created by Andrey Belkin on 10/07/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import RxSwift

open class BaseInteractor<RESULT, ENTITY> {
    
    public func getData(params:RequestParameters.Type)  -> Observable<RESULT>
    {
        fatalError("fatal error")
    }
}
