//
//  BaseStorageInteractor.swift
//  CleanCore_DI
//
//  Created by Andrey Belkin on 26/05/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import RxSwift

open class BaseStorageInteractor<RESULT, ENTITY>: BaseInteractor<RESULT, ENTITY> {
    
    public  let service: BaseService<RESULT>
    
    public init(service:BaseService<RESULT>)
    {
        self.service = service
    }
    
    override open func getData(params:RequestParameters.Type) -> Observable<RESULT>
    {
        return self.service.getData(params: params)
    }
}
