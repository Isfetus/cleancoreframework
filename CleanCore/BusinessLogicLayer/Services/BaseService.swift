//
//  File.swift
//  CleanCore
//
//  Created by Andrey Belkin on 10/07/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import Foundation
import RxSwift

public class BaseService<DATA>
{
    public func getData(params:RequestParameters.Type) -> Observable<DATA>
    {
        fatalError("abstract method")
    }
}



