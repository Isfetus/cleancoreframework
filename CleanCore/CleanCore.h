//
//  CleanCore.h
//  CleanCore
//
//  Created by Andrey Belkin on 29/07/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CleanCore.
FOUNDATION_EXPORT double CleanCoreVersionNumber;

//! Project version string for CleanCore.
FOUNDATION_EXPORT const unsigned char CleanCoreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CleanCore/PublicHeader.h>


