//
//  Network.swift
//  SwinjectMVVMExample
//
//  Created by Yoichi Tagaya on 8/22/15.
//  Copyright © 2015 Swinject Contributors. All rights reserved.
//

import Alamofire
import RxSwift
import UIKit

fileprivate let validStatusCodes:CountableRange<Int> = 200..<300

public final class Network: Networking
{
    private let queue = DispatchQueue(label: "CleanCore.Network.Queue", attributes: [])
    
    public init() { }
   
    public func requestJSON(url: String, method:HTTPMethod, parameters: [String : Any]?, headers:[String : String]? = nil) -> Observable<Any> {
        
        return Observable.create { observer in
            
            Alamofire.request(url, method: method, parameters: parameters, headers: headers)
                .validate(statusCode: validStatusCodes)
                .response(queue: self.queue, responseSerializer: Alamofire.DataRequest.jsonResponseSerializer())
                { response in

                    DispatchQueue.main.async
                    {
                        switch response.result
                        {
                            case .success(let value):

                                observer.on(.next(value))
                                observer.on(.completed)
                            
                            case .failure(let error):

                                observer.on(.error(error as NSError))
                        }
                    }
                }
            
            return Disposables.create()
        }
    }
    
    public func requestImage(_ url: String) -> Observable<UIImage>
    {
        return Observable.create { observer in
            
            Alamofire.request(url, method: .get)
            .response(queue: self.queue, responseSerializer: Alamofire.DataRequest.dataResponseSerializer()) { response in
                        
                switch response.result {
                
                    case .success(let data):
                        
                        guard let image = UIImage(data: data)
                        else
                        {
                            observer.on(.error(NSError(domain: NSURLErrorDomain, code: NSURLErrorBadServerResponse, userInfo: nil)))
                            return
                        }
                        
                        observer.on(.next(image))
                        observer.on(.completed)
                        
                    case .failure(let error):
                        
                        observer.on(.error(error as NSError))
                }
            }
            
            return Disposables.create()
        }
     }
}
