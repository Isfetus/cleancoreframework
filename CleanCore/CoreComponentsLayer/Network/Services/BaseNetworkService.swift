//
//  NetworkService.swift
//  CleanCore
//
//  Created by Andrey Belkin on 29/06/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import RxSwift
import SwiftyJSON

public class BaseNetworkService<DATA>: BaseService<DATA> {

    internal let network: Networking
    internal let responseMapper: ResponseMapper.Type
    
    public init(networking:Networking, responseMapper:ResponseMapper.Type)
    {        
        self.network = networking
        self.responseMapper = responseMapper
    }
    
    public override func getData(params: RequestParameters.Type) -> Observable<DATA>
    {
        return self.network.requestJSON(url: params.apiURL,
                                        method: params.method,
                                        parameters: params.requestParameters,
                                        headers: params.headers)
                                        .map({ (response:Any) -> DATA in
                                        
                                            let json = JSON(response)
                                            let data = self.responseMapper.parse(responseJSON: json)
                                        
                                            return data as! DATA
                                        })
                                        .retry(3)
                                        .timeout(10, scheduler: MainScheduler.instance)
    }
}
