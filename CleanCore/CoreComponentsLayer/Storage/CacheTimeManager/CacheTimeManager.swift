//
//  CacheManager.swift
//  CleanCore
//
//  Created by Andrey Belkin on 29/05/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
fileprivate let CACHE_TIMES_DICTIONARY_KEY = "CacheTimes"

public class CacheTimeManager
{
    static let shared = CacheTimeManager.init()
    
    var cacheTimes = [String: Date]()

    private init()
    {
        if UserDefaults.standard.dictionary(forKey: CACHE_TIMES_DICTIONARY_KEY) == nil
        {
            UserDefaults.standard.set([String:Date](), forKey: CACHE_TIMES_DICTIONARY_KEY)
        }
    }
    
    func dataIsCachedAndValid(cachePath: String, cacheInterval:TimeInterval) -> Bool
    {
        let cacheTimes:[String:Date] = UserDefaults.standard.dictionary(forKey: CACHE_TIMES_DICTIONARY_KEY) as! [String : Date]
        if let cacheDate = cacheTimes[cachePath]
        {
            return isCacheDateValid(cacheDate: cacheDate, cacheInterval: cacheInterval)
        }
        else
        {
            return false
        }
    }
    
    func updateCacheTime(cachePath: String)
    {
        var cacheTimes:[String:Date] = UserDefaults.standard.dictionary(forKey: CACHE_TIMES_DICTIONARY_KEY) as! [String : Date]
        cacheTimes[cachePath] = Date()
        
        UserDefaults.standard.set(cacheTimes, forKey: CACHE_TIMES_DICTIONARY_KEY)
    }
    
    func isCacheDateValid(cacheDate: Date, cacheInterval:TimeInterval) -> Bool
    {
        let nowDate = Date()
        let calculatedInterval = nowDate.timeIntervalSince(cacheDate)
        
        if calculatedInterval < cacheInterval
        {
            return true
        }
        
        return false
    }
    
    func deleteCache()
    {
        UserDefaults.standard.set([String:Date](), forKey: CACHE_TIMES_DICTIONARY_KEY)
    }
}
