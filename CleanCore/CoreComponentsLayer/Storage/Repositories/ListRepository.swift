//
//  ListRepository.swift
//  CleanCore
//
//  Created by Andrey Belkin on 12/07/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import Foundation
import CoreData
import RxSwift

open class ListRepository<DATA, DAO>: AbstractRepository<[DATA], DAO> where DATA:PlainObject, DAO: PlainConvertibleType, DAO: NSFetchRequestResult, DAO.PlainType == DATA {

    private var scheduler: ContextScheduler
    
    override public init(context: NSManagedObjectContext)
    {
        self.scheduler = ContextScheduler(context: context)
        
        super.init(context: context)
    }    

    override public func getData(predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = []) -> Observable<[DATA]>
    {
        let request = NSFetchRequest<DAO>(entityName: String(describing: DAO.self))
        request.predicate = predicate
        request.sortDescriptors = sortDescriptors

        return context.rx.entities(fetchRequest: request)
            .mapToPlain()
    }
    
    override public func save(data:[DATA]) -> Observable<Void>
    {
       let request = NSFetchRequest<DAO>(entityName: String(describing: DAO.self))
        
        return self.context.rx.delete(fetchRequest: request)
            .flatMap { return self.context.rx.add(entities: data) }
            .flatMap { return self.context.rx.save().subscribeOn(self.scheduler) }       
    }

    public func delete() -> Observable<Void>
    {
        let request = NSFetchRequest<DAO>(entityName: String(describing: DAO.self))
        
        return self.context.rx.delete(fetchRequest: request)
            .flatMap { return self.context.rx.save().subscribeOn(self.scheduler) }
    }
}
