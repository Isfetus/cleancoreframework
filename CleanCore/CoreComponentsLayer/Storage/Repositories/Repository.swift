import Foundation
import CoreData
import RxSwift

func abstractMethod() -> Never {
    fatalError("abstract method")
}

open class Repository<DATA, DAO> : AbstractRepository<DATA, DAO> where DATA:PlainObject, DAO: PlainConvertibleType, DAO: NSFetchRequestResult, DAO.PlainType == DATA{

    private let scheduler: ContextScheduler

    public override init(context: NSManagedObjectContext)
    {
        self.scheduler = ContextScheduler(context: context)
        
        super.init(context: context)        
    }

    override public func getData(predicate: NSPredicate? = nil, sortDescriptors: [NSSortDescriptor]? = []) -> Observable<DATA>
    {
        let request = NSFetchRequest<DAO>(entityName: String(describing: DAO.self))
        request.predicate = predicate
        request.sortDescriptors = sortDescriptors
        
        return context.rx.entities(fetchRequest: request)
            .mapToPlain()
            .flatMap({ (plainData:[DATA]) -> Observable<DATA> in
                
                if plainData.count > 0
                {
                    return Observable.just(plainData[0])
                }
                else
                {
                    return Observable<DATA>.error(RxError.noElements)
                }
            })
    }

    override public func save(data: DATA) -> Observable<Void>
    {
        return data.sync(in: context)
            .mapToVoid()
            .flatMapLatest(context.rx.save)
            .subscribeOn(scheduler)
    }
    
    public func delete() -> Observable<Void>
    {
        let request = NSFetchRequest<DAO>(entityName: String(describing: DAO.self))
        
        return self.context.rx.delete(fetchRequest: request)
            .flatMap { return self.context.rx.save().subscribeOn(self.scheduler) }
    }
}
