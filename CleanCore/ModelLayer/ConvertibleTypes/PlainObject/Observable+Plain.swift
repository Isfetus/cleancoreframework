import Foundation
import RxSwift

extension ObservableType
{
    public func mapToVoid() -> Observable<Void>
    {
        return map { _ in }
    }
}

extension Observable where Element: Sequence, Element.Iterator.Element: PlainConvertibleType
{
    typealias PlainType = Element.Iterator.Element.PlainType
    
    func mapToPlain() -> Observable<[PlainType]>
    {
        return map { sequence -> [PlainType] in
            
            return sequence.mapToPlain()
        }
    }
}

extension Sequence where Iterator.Element: PlainConvertibleType
{
    public typealias Element = Iterator.Element
    public func mapToPlain() -> [Element.PlainType]
    {
        return map
        {
            return $0.asPlain()
        }
    }
}
