import Foundation
import CoreData
import RxSwift
import SwiftyJSON

public protocol PlainConvertibleType
{
    associatedtype PlainType

    func asPlain() -> PlainType
}

public protocol Decodable
{    
    static func plainFromJSON(json:JSON) -> Self
}


