import Foundation
import CoreData

open class CoreDataStack
{
    internal let storeCoordinator: NSPersistentStoreCoordinator
    public let context: NSManagedObjectContext
    let contextPath: String

    public init(contextPath:String)
    {        
        self.contextPath = contextPath
        
        let bundle = Bundle(for: type(of: self).self)
        guard let url = bundle.url(forResource: contextPath, withExtension: "momd"),
              let model = NSManagedObjectModel(contentsOf: url)
        else
        {
            fatalError()
        }
        
        self.storeCoordinator = NSPersistentStoreCoordinator(managedObjectModel: model)
        self.context = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        self.context.persistentStoreCoordinator = self.storeCoordinator
        self.migrateStore()
    }

    private func migrateStore()
    {
        guard let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
        else
        {
            fatalError()
        }
        
        let storeUrl = url.appendingPathComponent("\(contextPath).sqlite")
        
        do
        {
            try storeCoordinator.addPersistentStore(ofType: NSSQLiteStoreType,
                    configurationName: nil,
                    at: storeUrl,
                    options: nil)
        }
        catch
        {
            fatalError("Error migrating store: \(error)")
        }
    }
}
