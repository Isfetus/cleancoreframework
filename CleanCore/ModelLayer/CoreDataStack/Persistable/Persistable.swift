import Foundation
import CoreData
import RxSwift

public protocol Persistable: NSFetchRequestResult, PlainConvertibleType
{    
    static var entityName: String {get}
    static func fetchRequest() -> NSFetchRequest<Self>
}


extension Persistable
{
    static var primaryAttribute: String { return "id" }
}

