//
//  BaseViewModel.swift
//  CleanCore
//
//  Created by Andrey Belkin on 10/07/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import RxSwift

open class BaseViewModel<RESULT, ENTITY>: NSObject {
    
    public let interactor:BaseInteractor<RESULT, ENTITY>
    public var disposeBag:DisposeBag? = DisposeBag()
    
    public var isLoading: BehaviorSubject<Bool> = BehaviorSubject.init(value: false)
    
    public init(interactor:BaseInteractor<RESULT, ENTITY>)
    {
        self.interactor = interactor
        
        super.init()
    }
    
    public func subscribeToData(params: RequestParameters.Type) -> Observable<RESULT>
    {
        self.isLoading.onNext(true)

        return self.interactor.getData(params: params).do(
            
            onNext: { (data:RESULT) in

                self.isLoading.onNext(false)
            
            },
            onError: { (error:Error) in
            
                self.isLoading.onNext(false)
            }
        )
    }
    
    public func unsubscribeFromData()
    {
        disposeBag = DisposeBag()
    }
    
    public func parseModels(data:Observable<RESULT>) {}
}
