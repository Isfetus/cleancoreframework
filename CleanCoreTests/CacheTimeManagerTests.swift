//
//  CacheTimeManagerTests.swift
//  CleanCore
//
//  Created by Andrey Belkin on 09/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//


import XCTest
import Nimble
import Quick

class CacheTimeManagerTests: QuickSpec
{
    override func spec()
    {
        it("cache for path isn't exist")
        {
            let cacheForPathIsExist = CacheTimeManager.shared.dataIsCachedAndValid(cachePath: "path", cacheInterval: 3600)
            expect(cacheForPathIsExist).to(equal(false))
        }
        
        it("cache for path is exist")
        {
            let testCachePath = "testCachePath"
            
            CacheTimeManager.shared.updateCacheTime(cachePath: testCachePath)
            let cacheForPathIsExist = CacheTimeManager.shared.dataIsCachedAndValid(cachePath: testCachePath, cacheInterval: 3600)
            expect(cacheForPathIsExist).to(equal(true))
        }
        
        it("cache date is valid")
        {
            var testDate = Date()
            testDate.addTimeInterval(-100)
            
            let cacheIsValid = CacheTimeManager.shared.isCacheDateValid(cacheDate: testDate, cacheInterval: 50)
            expect(cacheIsValid).to(equal(false))
        }
        
        it("cache date is invalid")
        {
            var testDate = Date()
            testDate.addTimeInterval(-100)
            
            let cacheIsValid = CacheTimeManager.shared.isCacheDateValid(cacheDate: testDate, cacheInterval: 110)
            expect(cacheIsValid).to(equal(true))
        }       
    }
    
    override func tearDown()
    {
        super.tearDown()
    }
}

