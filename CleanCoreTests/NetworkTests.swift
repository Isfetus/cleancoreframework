//
//  NetworkTests.swift
//  CleanCore
//
//  Created by Andrey Belkin on 02/08/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import XCTest
import OHHTTPStubs
import Alamofire
import RxSwift
import Nimble
import Quick

class NetworkTests: QuickSpec {
    
    let testHost = "test.host"
    var disposeBag = DisposeBag()
    
    override func spec()
    {
        var network: Network!
        
        beforeEach
        {
            network = Network()
        }
        
        describe("Simple request with parameters")
        {
            it("eventually gets JSON data as specified with parameters.")
            {
                let getParameters:[String:String] = ["param":"1", "param2":"par"]
                let stubbedJSON = [
                                    "foo": "some text",
                                    "bar": "some other text",
                                    ]
                
                stub(condition: isHost(self.testHost) && isPath("/resources") && containsQueryParams(getParameters)) { _ in
                    return OHHTTPStubsResponse(
                        jsonObject: stubbedJSON,
                        statusCode: 200,
                        headers: .none
                    )
                }
                
                var resp:[String:String]? = nil
                network.requestJSON(url:"http://\(self.testHost)/resources", method: HTTPMethod.get, parameters: getParameters)
                .subscribe(
                    
                    onNext: { (response:Any) in
                        
                        resp = response as? [String:String]
                    }
                    
                ).addDisposableTo(self.disposeBag)
                
                expect(resp).toEventually(beNil(), timeout: 1)
                expect(resp).toEventually(equal(stubbedJSON), timeout: 1)
            }
        }
        
        describe("Request without parameters")
        {
            it("eventually gets JSON data as specified with parameters.")
            {
                let stubbedJSON = [
                    "foo": "some text",
                    "bar": "some other text",
                ]
                
                stub(condition: isHost(self.testHost) && isPath("/resources")) { _ in
                    return OHHTTPStubsResponse(
                        jsonObject: stubbedJSON,
                        statusCode: 200,
                        headers: .none
                    )
                }
                
                var resp:[String:String]? = nil
                network.requestJSON(url:"http://\(self.testHost)/resources", method: HTTPMethod.get, parameters: nil)
                .subscribe(
                    
                    onNext: { (response:Any) in
                        
                        resp = response as? [String:String]
                    }
                    
                ).addDisposableTo(self.disposeBag)
                
                expect(resp).toEventuallyNot(beNil(), timeout: 1)
                expect(resp).toEventually(equal(stubbedJSON), timeout: 1)
            }
        }
        
        describe("Request with error and user data")
        {
            it("Check error code in the response")
            {
                let stubbedJSON = [
                    "message": "Error text"
                ]
                
                let errorCode = 402
                
                stub(condition: isHost(self.testHost) && isPath("/error")) { _ in
                    
                    let notConnectedError = NSError(domain:NSURLErrorDomain, code:errorCode, userInfo:stubbedJSON)
                    return OHHTTPStubsResponse(error:notConnectedError)
                }
                
                var responseError:NSError? = nil
                network.requestJSON(url:"http://\(self.testHost)/error", method: HTTPMethod.get, parameters: nil)
                .subscribe(
                    
                    onError: { (error:Error) in
                        
                        responseError = error as NSError
                    }
                    
                ).addDisposableTo(self.disposeBag)
               
                expect(responseError?.code).toEventually(equal(errorCode), timeout: 1)
                expect(responseError).toEventuallyNot(beNil(), timeout: 1)
                expect(responseError?.userInfo).toEventuallyNot(beNil(), timeout: 1)
                expect(responseError?.userInfo as? [String:String]).toEventually(equal(stubbedJSON), timeout: 1)
            }
        }
        
        describe("Request if server path is wrong")
        {
            it("handle down network error")
            {
                stub(condition: isHost(self.testHost) && isPath("/networkerror")) { _ in
                    
                    let notConnectedError = NSError(domain:NSURLErrorDomain, code:NSURLErrorNotConnectedToInternet, userInfo:nil)
                    return OHHTTPStubsResponse(error:notConnectedError)
                }
                
                var resultCode:Int32 = 0
                network.requestJSON(url:"http://\(self.testHost)/networkerror", method: HTTPMethod.get, parameters: nil)
                .subscribe(
                    
                    onError: { (error:Error) in
                        
                        resultCode = Int32((error as NSError).code)
                    }
                    
                ).addDisposableTo(self.disposeBag)                
                
                expect(resultCode).toEventually(equal(CFNetworkErrors.cfurlErrorNotConnectedToInternet.rawValue), timeout: 1)
            }
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        OHHTTPStubs.removeAllStubs()
        
        super.tearDown()
        
        self.disposeBag = DisposeBag()
    }
}
