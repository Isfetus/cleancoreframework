//
//  StoreDetail.swift
//  CleanCore
//
//  Created by Andrey Belkin on 13/07/17.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit

import Foundation
import SwiftyJSON
import CleanCore

public struct TestData: PlainObject
{
    public let id: Int32
    public let param1: String?
    public let param2: Double
    public let param3: String?
    
    public init(id: Int32, param1: String?, param2:Double, param3: String?)
    {
        self.id = id
        self.param1 = param1
        self.param2 = param2
        self.param3 = param3
    }
}

extension TestData
{
    public typealias CoreDataType = TestDataEntity
    
    public func mapToEntity(entity: TestDataEntity)
    {
        entity.id = id
        entity.param1 = param1
        entity.param2 = param2
        entity.param3 = param3
    }
}

extension TestData
{
    public static func plainFromJSON(json:JSON) -> TestData
    {
        return TestData(
            id: json["id"].int32!,
            param1: json["param1"].string,
            param2: json["param2"].double!,
            param3: json["param3"].string
        )
    }
}

// MARK: For testing
extension TestData: Equatable
{
    public static func == (lhs: TestData, rhs: TestData) -> Bool
    {        
        return
            lhs.id == rhs.id &&
                lhs.param1 == rhs.param1 &&
                lhs.param2 == rhs.param2 &&
                lhs.param3 == rhs.param3
    }
}
