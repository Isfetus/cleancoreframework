//
//  TestData+Extension.swift
//  CleanCore
//
//  Created by Andrey Belkin on 27/07/2017.
//  Copyright © 2017 Andrey Belkin. All rights reserved.
//

import UIKit
import CleanCore

extension TestDataEntity: PlainConvertibleType
{
    public func asPlain() -> TestData
    {
        return TestData(id: id, param1: param1, param2: param2, param3: param3)
    }
}

extension TestDataEntity: Persistable
{
    public static var entityName: String
    {
        return "TestDataEntity"
    }
}
